var path = require('path');
var merge = require('webpack-merge');
var webpack = require('webpack');

var TARGET = process.env.npm_lifecycle_event;

var PATHS = {
  app: root('app/'),
  build: root('target/build')
};

function root(p) {
    return path.join(__dirname,p);
}

var common = {
  // Entry accepts a path or an object of entries. We'll be using the
  // latter form given it's convenient with more complex configurations.
  entry: {
    angular: ['es7-reflect-metadata/dist/browser',
        // without the pollyfils component updates are not wokring
        'angular2/bundles/angular2-polyfills',
        // angular2 entry point
        'angular2/platform/browser',
        'rxjs/Rx',
        'angular2/http'],
    app: path.join(PATHS.app, 'index.ts'),
    experiment: path.join(PATHS.app, 'experiment/index.ts')
  },
  resolve: {
    // ensure loader extensions match. empty string is essential!
    extensions: ["", ".webpack.js", ".web.js", ".js",'.ts', '.css']
  },
  module: {
      loaders: [
          { test: /\.ts$/, loader: 'ts-loader', include: PATHS.app },
          { test: /\.css$/, loaders: ['style', 'css'], include: PATHS.app }
      ],
      preLoaders: [
          { test: /\.js$/, loader: "source-map-loader", exclude: [ root('node_modules/rxjs') ] }
      ]      
  },
  
  output: {
    path: PATHS.build,
    publicPath: 'bundle/',
    filename: '[name].js',
    sourceMapFilename: '[name].map',
    chunkFilename: '[id].chunk.js'    
  },
  
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({ name: 'angular', filename: 'angular.js', minChunks: Infinity}),      
  ]
};

if(TARGET === 'start' || !TARGET) {
  module.exports = merge(common, {
    // custom configuration below
    //devtool: 'source-map', // survivejs says eval-source-map
    
    devServer: {
      contentBase: 'app',
      
      // Enable history API fallback so HTML5 History API based
      // routing works. This is a good default that will come
      // in handy in more complicated setups.
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,

      // Display only errors to reduce the amount of output.
      stats: 'errors-only',

      // Parse host and port from env so this is easy to customize.
      //
      // If you use Vagrant or Cloud9, set
      // host: process.env.HOST || '0.0.0.0';
      //
      // 0.0.0.0 is available to all network devices unlike default
      // localhost
      host: process.env.HOST,
      port: process.env.PORT || 3000
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.SourceMapDevToolPlugin ({
         exclude: ['angular.js'], // source maps take lots of time
         module: true // use SourceMaps from loaders 
         })
    ]
  });      
}

if(TARGET === 'build') {
  module.exports = merge(common, {});
}
