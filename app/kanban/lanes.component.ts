import {Component,Input,Output} from 'angular2/core';
import * as m from '../model/model';
import {LaneComponent} from './lane.component';

@Component({
    selector: `lanes`,
    template: `<div class="lanes">
      <lane *ngFor="#lane of lanes" [lane]="lane" class="lane"></lane>
    </div>`,
    directives: [LaneComponent]
})
export class LanesComponent {
    @Input() lanes:m.Lane[];
}