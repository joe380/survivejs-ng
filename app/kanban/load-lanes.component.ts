/*
 * Angular
 */
import {Component} from 'angular2/core';
import {BackendConnectionService} from "../model/backend-connection.service";

@Component({
  selector: 'load-lanes',
  template: `<button type="button" (click)="makeRequest()">Make Request</button>`
})
export class LoadLanesComponent {

  constructor(public backend: BackendConnectionService) {
  }

  makeRequest(): void {
    this.backend.loadLanes();
  }
}

