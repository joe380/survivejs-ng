import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, 
        AfterViewInit, ElementRef, ViewChild} from 'angular2/core';


@Component({
    selector: 'view-note',
    template: '<div><span (click)="onEdit()" class="task">{{task}}</span>\n\
               <button (click)="doDelete()" class="delete-note">x</button></div>'
})
class ViewNoteComponent {
    @Input() task;
    @Output() delete = new EventEmitter();
    @Output() edit = new EventEmitter();
    
    doDelete() {
        this.delete.emit(null);
    }
    
    onEdit() {
        this.edit.emit(null);
    }
}

@Component({
    selector: 'edit-note',
    styles: ["input { width: 100%}"],
    template: `<input #in type="text" [value]="task"
     (blur)="finish(in.value)" (keyup.Enter)="finish(in.value)" (keyup.Escape)="finish(task)">`
})
class EditNoteComponent implements AfterViewInit {
    @Input() task;
    // https://github.com/angular/angular/issues/6786#issuecomment-189721097
    @Output() editFinished = new EventEmitter<String>(false);
    
    // this is how you get reference to template
    @ViewChild('in') input:ElementRef;
    
    // view child is valid after view init    
    ngAfterViewInit() {
        const e: HTMLInputElement = this.input.nativeElement;
        e.select();
        e.focus();
    }
    
    finish(text:string) {
        if (!this.editFinished.isUnsubscribed) {
            this.editFinished.emit(text);
            this.editFinished.complete();
        }
    }
    
}

@Component({
    selector: 'note',
    template: '<div [ngSwitch]="editing" >\n\
       <view-note *ngSwitchWhen="false" (edit)="toggleEdit()" [task]="task" (delete)="delete.emit(null)"></view-note>\n\
       <edit-note *ngSwitchDefault [task]="task" (editFinished)="finish($event)"></edit-note></div>',
    directives: [ViewNoteComponent, EditNoteComponent],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoteComponent {
    editing = false;
    @Input() task;
    @Output() editFinished = new EventEmitter<String>();
    @Output() delete = new EventEmitter();
    
    toggleEdit() {
        this.editing = !this.editing;
    }
    
    finish(text:string) {
        this.editFinished.emit(text);
        this.toggleEdit();
    }
}