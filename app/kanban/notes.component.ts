import {Component, Input, Output, EventEmitter} from 'angular2/core';
import {NoteComponent} from './note.component';
import * as m from '../model/model';

@Component({
    selector: 'notes',
    template: `<ul class="notes">
        <li *ngFor="#n of notes" class="note">
          <note [task]="n.task"
            (editFinished)="edit(n.id, $event)"
            (delete)="onDelete(n.id)">
          </note></li>
      </ul>`,
    directives: [NoteComponent]
})
export class NotesComponent {
    @Input() notes : m.Note[];
    
    // We'd like to emit Note - the model object, butthe types collide here.
    // Therefore Angular recommends Component suffix.
    @Output() editFinished = new EventEmitter();
    @Output() delete = new EventEmitter<String>();
    
    edit(id:string, text:string) {
        console.log("Notes.edit", id);
        this.editFinished.emit({id:id, task:text});
    }
    
    onDelete(id:string) {
        this.delete.emit(id);
    }
}