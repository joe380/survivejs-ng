import {Input,Component,provide,SkipSelf,Injector} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';


abstract class DataProvider {
    abstract getData(): string
} 

export class DefaultProvider {
    getData() { return "42" }
}

@Component({
    selector: 'child',
    template: '<div>a child has data of {{data.getData()}}</div>'
})
class Child {
    constructor(private data:DataProvider) {
        console.log("The provider is ", data, " and data is ", data.getData());
    }
}

@Component({
    selector: 'parent',
    template: '<div>The parent has data of {{data.getData()}}</div><child></child>',
    
    providers: [provide( DataProvider, {
        useFactory: (c:Parent) => c.dataProvider(),
        deps: [Parent]})],
    directives: [Child]
})
class Parent {
    @Input() customData: string;
    
    constructor( @SkipSelf() private data: DataProvider) {
        console.log("Parent provided data is ", this.data.getData());
    }
    
    dataProvider() {
        class ProviderImpl extends DataProvider {
            constructor(private self:Parent) {super();}
            getData() { return this.self.customData }
        }
        return new ProviderImpl(this);
    }
}

@Component({
    selector: 'app',
    template: '<parent customData="45"></parent>',
    providers: [provide(DataProvider, {useClass: DefaultProvider})],
    directives: [Parent]
})
export class App {}

bootstrap(App).catch(err => console.error(err));
