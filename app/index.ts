//library imports: shims, pollyfills, etc
import 'es7-reflect-metadata/dist/browser';

//angular imports
import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';

//app imports
import './main.css'
import {AppComponent} from './app.component';
import {store} from './store';
import {providers} from './model/services'

bootstrap(AppComponent, [HTTP_PROVIDERS, providers, store]).catch(err => console.error(err));