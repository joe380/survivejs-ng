import {Injectable} from 'angular2/core';
import {Lane} from './model';
import * as a from './actions';
import * as uuid from 'node-uuid';

@Injectable()
export class LaneActions {
    constructor(private dispatch:a.Dispatcher) {}
    
    createLane(name:string) {
        this.dispatch.next(a.createLane({name}));
    }
    
    addNote(task:string, lane:Lane) {
        this.dispatch.next(a.addNote({task, laneId:lane.id}));
    }
    
    deleteLane(id:string) {
        this.dispatch.next(a.deleteLane({id}));
    }
    
    editLaneName(id:string, name:string) {
        this.dispatch.next(a.editLaneName({id, name}));
    }
}

class LaneReducerM implements a.ReduceMethods<Lane[]> {
    
    constructor(private s:Lane[]) {}
    
    createLane(p: typeof a.createLane.payload) {
        const lane = {
            id: uuid.v4(),
            name: p.name,
            notes: []
        };
        const s = this.s || [];
        return s.concat(lane);
    }
    
    addNote(p: typeof a.addNote.payload) {
        if (p.laneId) {
            return this.attachToLane(p.laneId, p.tempId);
        } else {
            return this.s;
        }
    }

    lanesLoaded(lanes:Lane[]) {
        return lanes;
    }
    
    deleteLane(p: {id:string}) {
        return this.s.filter(lane => lane.id !== p.id);
    }
    
    editLaneName(p: {id:string, name:string}) {
        return this.s.map(lane=> lane.id === p.id ? Object.assign({}, lane, {name: p.name}) : lane);
    }
      
    private attachToLane(laneId:string, noteId:string) {
        return this.s.map(lane => {
            if(lane.id === laneId) {
              if (lane.notes.indexOf(noteId) >= 0) {
                console.warn('Already attached note to lane', lane);
              } else {
                console.log("Attaching note",noteId);
                lane.notes.push(noteId);
              }
            }
            return lane;
        });
    }
    
    private detachFromLane(laneId:string, noteId:string) {
        return this.s.map(lane => {
            if(lane.id === laneId) {
                lane.notes = lane.notes.filter(note => note !== noteId);
            }
            return lane;
        });
    }
}

export const LaneReducer: a.ActionReducer<Lane[]> = s => new LaneReducerM(s);