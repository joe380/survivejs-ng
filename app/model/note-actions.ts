import {Injectable} from 'angular2/core';
import {Note} from './model'
import * as a from './actions'

@Injectable()
export class NoteActions {
    
    constructor(private dispatch:a.Dispatcher) { }
    
    editNote(n:Note) {
        this.dispatch.next(a.editNote(n));
    }
    
    addNote(text:string) {
        console.log("Dispatching add",text);
        this.dispatch.next(a.addNote({task: text}));
    }
    
    deleteNote(id:string) {
        this.dispatch.next(a.deleteNote({id}));
    }
}

// actually a function, but with this notation we can verify the
// return value correctnes here as well as type of s.
export const NotesReducer:a.ActionReducer<Note[]> = function(s) { 
    return {
        addNote(p:typeof a.addNote.payload) {
            console.log("Reducer adding",p);
            const note = {
                id: p.tempId, task: p.task};
            return [...s, note];
        },
        
        editNote(p: typeof a.editNote.payload) {
            // es6 shim needs to be referred to support Object.assign I suppose
            return s.map(n => n.id === p.id ? p : n)
        },
        
        deleteNote(p: typeof a.deleteNote.payload) {
            return s.filter(n => n.id != p.id);
        }
    };
};


